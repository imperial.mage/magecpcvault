# Spawn Open Source

I made this simlpe tool to use higher tracks on 3'1/2 floppy to store twice more things on them.

As far as I remember, it uses a lot of Antoine FDC routines.

This release is GPLv3.

Included file "Spawn_v1.0.dsk" courtesy of [CPC Rulez](https://cpcrulez.fr/applications_disc-spawn.htm) (Thank you)

Included file "Spawn_v1.3.dsk" is my own compilation of the latest source I have (I can't recall what changed since 1.0) 

# Compile "as is" on CPC using DAMS

Load DAMS at #400 then use these command to compile and save :
```
M 32300
G,D-SPAWN
    #Change the "SAUVE" variable to 0 at the beginning.
A2
P2,-SPAWN
```
Reset then run Spawn :
```
run"-spawn"
```


# First try to compile using RASM

[First, dowload and install DAMSDECODE from Pascal SEGUY](https://github.com/pseguy/dams)

Transform the source from binary to ASCII :
```
damsdecode -S < D-SPAWN.BIN > Spawn-brut.asm
cp Spawn-brut.asm Spawn-2022.asm
```

[Then download and Install RASM From Edouard BERGE](https://github.com/EdouardBERGE/rasm)

Then proceed to some "generic" substitutions :

```
# on remplace toutes les occurences de ,, par ,0, car c'est comme ça que DAMS les comprend
gsed -i 's/,,/,0,/g' Spawn-2022.asm
gsed -i 's/,,/,0,/g' Spawn-2022.asm
gsed -i 's/DEFB	,/DEFB	0,/g' Spawn-2022.asm
gsed -i 's/,$/,0/g' Spawn-2022.asm

# substitute some keywords to RASM
gsed -i  's/PRINT/AFFICHE/' Spawn-2022.asm
gsed -i  's/STOP/ARRET/' Spawn-2022.asm
gsed -i  's/PAUSE/PAUSEMAJ/' Spawn-2022.asm
gsed -i  's/NOM/NOMMAJ/' Spawn-2022.asm

gsed -i  's/DEFB$/DEFB\t0/' Spawn-2022.asm
gsed -i  's/DEFW$/DEFW\t0/' Spawn-2022.asm

# on entoure toutes les lignes DEFM avec des guillemets
gsed -i  's/DEFM\t/DEFM\t"/' Spawn-2022.asm
gsed -i  '/DEFM/ s/$/"/' Spawn-2022.asm
```

These operations are specific to this source :

```
#Si on ne décommente pas le bloc du début, corriger le END => ENDIF 
gsed -i  's/END/ENDIF/' Spawn-2022.asm

echo "RUN DCA" >> Spawn-2022.asm
echo 'SAVE "-spawn.007",debut,fin-debut,DSK,"fichier.dsk"' >> Spawn-2022.asm

#On commente le bloc du debut qui sauvegardait
gsed -i  '2,38s/^/;/' Spawn-2022.asm
```

# Transform and compile using third party tools

I lack of time. Some hint were given to me by AST : Dams Converter (By Antoine!), Orgams (By Madram). Feel free to send me your suggestions and modus operandi.
