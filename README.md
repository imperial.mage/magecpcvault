# Mage CPC Vault

A small place to share some stuff about Amstrad CPC. In the past month I exhumated some stuff that triggered my nostalgia mood.

Most of this stuff is of course outdated, ben maybe it could be worth to one or two people as pieces of CPC history.



# 2023 03 03 - First release : Spawn 1.3 goes open source

[Please check the related directory :)](https://gitlab.com/imperial.mage/magecpcvault/-/tree/main/2023-03-03-Spawn-GPL)

# Greetings

Message Personnel - Françoise Hardy asks : "Where are you ?"

## IRL Amstrad CPC Fellows

People i shook hand and delightly chatted with : Sébastien Garcia, Alexis Henaux, Pierre Leprun, Carlos Pardo, Antoine Pitrou, Serge Querné

## Amstrad CPC Greetings 

Over the years I especially appricated demos from those people : Antoine, Barthy, BSC, Digit, Dr TKC, Face Hugger, Fefesse, Gozeur, Longshot, MCS, Naminu, New Way Cracking, Overflow, Prodatron

"Junior sceeners" : I need time to really appreciate your work.

## Miscellaneous cool chat and reference from 2022

Il was nice to chat with both of you NoRecess and BDCIRON

- [Logon System : great work on the Amstrad Cpc Crtc Compendium & Shaker. Setting your work once again as a reference ](http://logonsystem.fr/)
- [Genesis 8 : thank you for continuous work over the years ! I always checked your site from time to time](https://genesis8bit.fr/)
- [CPC Power : great work. Improved my skills in captcha :)](https://www.cpc-power.com/)
- [CPC Rulez : thank you for you site and MaDe's files ](https://cpcrulez.fr)
- [Memory Full : great interviews. Of course my favourite one is Antoine's one :) ](http://memoryfull.net/)
- [Amstrad CPC Mémoire Ecrite ](https://acpc.me/)


## About Me

Yes, I'm the guy that pleaded guilty on all counts for releasing Divine "As Is", without finishing the menu nor the code and texts. Of course if I had not taken this decision, Divine would remain *unreleased* thought it had some cool and innovative stuff. 

After all those years, my favourite demos of mine might be : Time 2 Go, Dangerous (aka "Regulate"), and Best.
