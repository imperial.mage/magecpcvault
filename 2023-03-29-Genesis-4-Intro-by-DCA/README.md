# Genesis 4 dentro by DCA

This is probably my second *dentro*, just after "Victory Demo". I remember re-using my Victory Routines to make a quick intro.

Alexis Henaux was the founder of fanzine "Twilight Strad" that lived in my town. I met him after seeing an ad for "Twilight Strad" in CPC Infos #30. Alexis was also fan of music group **Genesis**, which led him to change his fanzine's name. Later we talked about Comics, Manage, the we paved the Road to Kimagure Orange.

Thank you to Amaury Durand (BDC IRON) for making a video rip and uploading the video [on his YouTube channel](https://youtu.be/olpXSLO2sl8)

Included : 
- [DSK File of **Genesis**](./Genesis-4-Intro-DCA.dsk)
- [CPC Info #30](./CPC_INFOS_30(acme).pdf), scanned by [Amstrad CPC Mémoire Ecrite ](https://acpc.me/)


![Genesis 4 screen shot]( ./GenesisScreen.png "title this :)")

